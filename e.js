"use strict";

var express = require('express');
var path = require('path');

var app = express();
var port = 8001;

app.use('/', express.static(path.join(__dirname, '/output')));

app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '/output/index.html'));
});


app.listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://localhost:' + port);
});
