import { EventEmitter } from 'events';
import Client from './mnsig';
import AppDispatcher from '../dispatcher';
import WalletConstants from '../constants/WalletConstants';


var CHANGE_EVENT = 'change';

// XXX Duplicate errorCode, userError

var _keys = {
  userError: null,  // If this becomes true, a logout will happen.
  errorCode: 0,
  data: null
};

var _transactions = {
  userError: null,
  errorCode: 0,
  data: null
};

var _export = {
  userError: null,
  errorCode: 0,
  data: null
};

function _clear(obj) {
  obj.userError = null;
  obj.errorCode = 0;
  obj.data = null;
}


function getKeys(err, json) {
  if (checkError(err, _keys)) {
    return;
  }
  _keys.data = json;
  WalletStore.emitChange();
}


function getTransactions(err, json) {
  if (checkError(err, _transactions)) {
    return;
  }
  _transactions.data = json;
  WalletStore.emitChange();
}


function exportTransactions(err, json) {
  if (checkError(err, _export)) {
    return;
  }
  _export.data = json;
  WalletStore.emitChange();
}


function checkError(err, obj) {
  if (!err) {
    return false;
  }

  const status = err.statusCode;
  obj.errorCode = status;
  if (status === 401) {
    obj.userError = true;
  }
  WalletStore.emitChange();
}


var WalletStore = Object.assign({}, EventEmitter.prototype, {

  getKeys: function() {
    return _keys;
  },

  getTransactions: function() {
    return _transactions;
  },

  getExport: function() {
    return _export;
  },

  reset: function() {
    _keys.userError = false;
    _transactions.userError = false;
    _export.userError = false;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

});


AppDispatcher.register(function(action) {

  switch (action.actionType) {
    case WalletConstants.GET_KEYS:
      _clear(_keys);
      Client.update(action).walletKeys(action.data, getKeys);
      break;

    case WalletConstants.GET_TRANSACTIONS:
      _clear(_transactions);
      Client.update(action).txlist(action.data, getTransactions);
      break;

    case WalletConstants.EXPORT_TRANSACTIONS:
      _clear(_export);
      Client.update(action).txlist(action.data, exportTransactions);
      break;

    default:
      // no op
  }

});


export default WalletStore;
