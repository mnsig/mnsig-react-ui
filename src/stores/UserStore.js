import store from 'store';
import { EventEmitter } from 'events';
import Client from './mnsig';
import AppDispatcher from '../dispatcher';
import UserConstants from '../constants/UserConstants';


var CHANGE_EVENT = 'change';

var _initialUser = {
  error: null,
  instance: null,

  created: false,
  token: null,
  username: null,
  tfaSetup: null,
  tfaMissing: null,
};

var _user = Object.assign({}, _initialUser, store.get('user'));
var _data = null;
var _apiToken = null;


function clearUser() {
  _data = null;
  _user = Object.assign({}, _initialUser);
  if (store.enabled) {
    store.set('user', _user);
  }
  Client.instance = null;
  Client.token = null;
}


function tryToLogin(err, json) {
  if (checkError(err, {'401': 'Double check your credentials'})) {
    Client.instance = null;
    return;
  }

  _user.error = null;
  _user.created = json.created;  // This is set during signup only.
  _user.token = json.token;
  _user.username = json.welcome;
  _user.tfaSetup = null;
  _user.tfaMissing = null;
  Client.token = _user.token;

  if (store.enabled) {
    store.set('user', _user);
  }

  if (!_user.token) {
    _user.tfaMissing = json.totp_required;
    if (!_user.tfaMissing) {
      _user.tfaSetup = {
        uri: json.totp_uri,
        secret: json.totp_secret,
        reset: json.totp_reset
      };
    }
  }

  UserStore.emitChange();
}


function logout() {
  clearUser();
  UserStore.emitChange();
}


function getUserData(err, json) {
  if (checkError(err)) {
    return;
  }
  _data = json;
  UserStore.emitChange();
}


function createWallet(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('new wallet', json);
  // _data = json;
  UserStore.emitChange();
}


function addKey(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('key put', json);
  UserStore.emitChange();
}


function deriveAddress(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('create address post', json);
  // if (!_json) {
  //   return;
  // }
  _data.lastAddress = json;
  UserStore.emitChange();
}


function createAPIToken(err, json) {
  if (checkError(err)) {
    return;
  }
  _apiToken = json;
  UserStore.emitChange();
}


function deleteAPIToken(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('delete token post', json);
  _data.removedToken = json;
  UserStore.emitChange();
}


function addWalletUser(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('add wallet user post', json);
  UserStore.emitChange();
}


function checkError(err, custom) {
  if (!err) {
    return false;
  }

  var custom = custom || {};
  const status = err.statusCode;
  if (status === -1 || status === 404) {
    clearUser();
    _user.error = 'Is your instance name correct?';
  } else if (status >= 400) {
    if (status === 401) {
      _user.error = (custom['401'] ? custom['401'] : 'Log in to continue');
      clearUser();
    } else if (status >= 500) {
      _user.error = 'Server failed to process the request';
    } else {
      _user.error = err.statusMessage;
    }
  } else {
    _user.error = err.statusMessage || 'Unknown error';
  }

  UserStore.emitChange();
  return true;
}


var UserStore = Object.assign({}, EventEmitter.prototype, {

  /**
   * Get the current user.
   * @return {object}
   */
  getUser: function() {
    return _user;
  },
  getData: function() {
    return _data;
  },
  getLastAPIToken: function() {
    var token = Object.assign({}, _apiToken);
    _apiToken = null;
    return token;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

});


AppDispatcher.register(function(action) {

  switch (action.actionType) {
    case UserConstants.DO_LOGIN:
      _user.instance = action.instance;
      Client.instance = _user.instance;
      action.data.hashedpwd = action.data.hashed_pwd;
      Client.login(action.data, tryToLogin);
      break;

    case UserConstants.DO_SIGNUP:
      _user.instance = action.instance;
      Client.instance = _user.instance;
      Client.signup(action.data, tryToLogin);
      break;

    case UserConstants.DO_LOGOUT:
      Client.logout(function(err, json) {
        console.log('logout', err, json)
      });
      logout();
      break;

    case UserConstants.GET_USERDATA:
      Client.update(action).userdata(getUserData);
      break;

    case UserConstants.CREATE_WALLET:
      Client.update(action).createWallet(action.data, createWallet);
      break;

    case UserConstants.ADD_KEY:
      Client.update(action).addWalletKey(action.data, addKey);
      break;

    case UserConstants.DERIVE_ADDRESS:
      Client.update(action).newAddress(action.data, deriveAddress);
      break;

    case UserConstants.CREATE_API_TOKEN:
      Client.update(action).addToken(action.data, createAPIToken);
      break;

    case UserConstants.DELETE_API_TOKEN:
      Client.update(action).deleteToken(action.data, deleteAPIToken);
      break;

    case UserConstants.ADD_WALLET_USER:
      Client.update(action).addWalletUser(action.data, addWalletUser);
      break;

    default:
      // no op
  }

});


export default UserStore;
