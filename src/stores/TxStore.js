import { EventEmitter } from 'events';
import Client from './mnsig';
import AppDispatcher from '../dispatcher';
import TxConstants from '../constants/TxConstants';


var CHANGE_EVENT = 'change';

var _tx = {
  userError: null,  // If this becomes true, a logout will happen.
  errorCode: 0,
  data: null
};


function _clear() {
  _tx.userError = null;
  _tx.errorCode = 0;
  _tx.data = null;
}


function startProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  _tx.data = json;
  TxStore.emitChange();
}


function prepareProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  _tx.data = json;
  TxStore.emitChange();
}


function cancelProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('cancel proposal', json);
  _tx.data = json;
  TxStore.emitChange();
}


function getProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('get proposal', json);
  _tx.data = json;
  TxStore.emitChange();
}


function signProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('sign proposal', json);
  _tx.data = json;
  TxStore.emitChange();
}


function broadcastProposal(err, json) {
  if (checkError(err)) {
    return;
  }
  console.log('broadcast proposal', json);
  _tx.data = json;
  TxStore.emitChange();
}



function checkError(err) {
  if (!err) {
    return false;
  }

  const status = err.statusCode;
  _tx.errorCode = status;
  if (status === 401) {
    _tx.userError = true;
  }
  _tx.data = {message: err.statusMessage};
  TxStore.emitChange();
}


var TxStore = Object.assign({}, EventEmitter.prototype, {

  /**
   * Get the current tx.
   * @return {object}
   */
  getTx: function() {
    return _tx;
  },

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

});


AppDispatcher.register(function(action) {

  switch (action.actionType) {
    case TxConstants.START_PROPOSAL:
      _clear();
      Client.update(action).startProposal(action.data, startProposal);
      break;

    case TxConstants.PREPARE_PROPOSAL:
      // This should commonly follow from startProposal.
      Client.update(action).prepareProposal(action.data, prepareProposal);
      break;

    case TxConstants.CANCEL_PROPOSAL:
      _clear();
      Client.update(action).deleteProposal(action.data, cancelProposal);
      break;

    case TxConstants.GET_PROPOSAL:
      _clear();
      Client.update(action).getProposal(action.data, getProposal);
      break;

    case TxConstants.SIGN_PROPOSAL:
      _clear();
      Client.update(action).localSign(action.data, action.xpriv, signProposal);
      break;

    case TxConstants.BROADCAST_PROPOSAL:
      _clear();
      Client.update(action).broadcast(action.data, broadcastProposal);
      break;

    default:
      // no op
  }

});


export default TxStore;
