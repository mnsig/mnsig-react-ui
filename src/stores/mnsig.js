import mnsig from 'mnsig-js-client';

var client = new mnsig.Client();
client.update = function(action) {
  client.instance = action.instance;
  client.token = action.token;
  return client;
}

export default client;
