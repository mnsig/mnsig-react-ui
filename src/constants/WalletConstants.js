import keyMirror from 'keymirror';

export default keyMirror({
  GET_KEYS: null,
  GET_TRANSACTIONS: null,
  EXPORT_TRANSACTIONS: null,
});
