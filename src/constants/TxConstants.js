import keyMirror from 'keymirror';

export default keyMirror({
  START_PROPOSAL: null,
  PREPARE_PROPOSAL: null,
  CANCEL_PROPOSAL: null,
  GET_PROPOSAL: null,
  SIGN_PROPOSAL: null,
  BROADCAST_PROPOSAL: null
});
