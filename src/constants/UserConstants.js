import keyMirror from 'keymirror';

export default keyMirror({
  DO_LOGIN: null,
  DO_SIGNUP: null,
  DO_LOGOUT: null,
  GET_USERDATA: null,
  CREATE_WALLET: null,
  ADD_KEY: null,
  DERIVE_ADDRESS: null,
  CREATE_API_TOKEN: null,
  DELETE_API_TOKEN: null,
  ADD_WALLET_USER: null
});
