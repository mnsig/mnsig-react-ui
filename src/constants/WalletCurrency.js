const WALLET_CURRENCY_CODE = {
    bitcoin: 'BTC',
    testnet: 'BTC',
    dash: 'DASH',
    dashtest: 'DASH',
};

export default WALLET_CURRENCY_CODE;
