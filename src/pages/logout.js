'use strict';

import React from 'react';
import { routerShape } from 'react-router';
import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


class Logout extends React.Component {

  componentDidMount() {
    UserActions.logout(UserStore.getUser());
    this.context.router.replace('/');
  }

  render() {
    return null;
  }
}

Logout.contextTypes = {
  router: routerShape.isRequired
};


export default Logout;
