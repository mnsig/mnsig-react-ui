'use strict';

import React from 'react';
import QRCode from 'qrcode.react';
import classNames from 'classnames';
import { routerShape } from 'react-router';

import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';


class Login extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      waiting: false,
      error: null,

      tfaSetup: null,
      tfaRequired: null,
      secretDisplayed: false
    };
  }

  componentDidMount() {
    this.formLogin = {};

    if (UserStore.getUser().token) {
      /* Already got an user. */
      return this.context.router.replace('/');
    }

    UserStore.addChangeListener(this._onUserChange);
    if (!this.refs.instance.readOnly) {
      this.refs.instance.focus();
    } else {
      this.refs.password.focus();
    }
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var user = UserStore.getUser();
    if (user.token) {
      /* Logged in! */
      const { location } = this.props;
      if (location.state && location.state.nextPathname) {
        this.context.router.replace(location.state.nextPathname);
      } else {
        this.context.router.replace('/');
      }
      return;
    } else if (user.created) {
      /* Completed sign up. */
      this.setState({waiting: false}, function() {
        this.context.router.replace('/login');
      });
      return;
    }

    this.refs.totp.value = '';
    this.setState({
      waiting: false,
      error: user.error,
      tfaSetup: user.tfaSetup,
      tfaRequired: user.tfaMissing
    });
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.setState({waiting: true, error: null});
    var instance = this.refs.instance ? this.refs.instance.value : this.formLogin.instance;
    var username = this.refs.username ? this.refs.username.value : this.formLogin.username;
    var password = this.refs.password ? this.refs.password.value : this.formLogin.password;
    var totp = this.refs.totp.value;
    var code = this.refs.magic ? this.refs.magic.value : null;
    var data = {'username': username};
    var path = code ? 'signup' : 'login';
    if (code) {
      data['magic'] = code;
    }
    if (totp) {
      data['totp'] = totp;
    }

    this.formLogin = {
      instance: instance,
      username: username,
      password: password
    };

    UserActions.login(instance, path, data, password);
  }

  onClickSecret(evt) {
    evt.preventDefault();
    var showing = !!this.refs.sharedsecret.textContent;

    if (showing) {
        this.refs.sharedsecret.textContent = '';
    } else {
        this.refs.sharedsecret.textContent = this.state.tfaSetup.secret;
    }

    this.setState({secretDisplayed: !showing});
  }

  render() {
    var title = 'Log in';
    var magic = '';
    var rawMagic = '';

    var magicTest = window.location.href.split('?');
    if (magicTest.length === 2) {
      var match = magicTest[1].match('code=(.+)');
      if (match !== undefined) {
        rawMagic = match[1];
        try {
          var raw = new Buffer(rawMagic.split('.')[0], 'base64').toString();
          magic = JSON.parse(raw);
          title = 'Sign up';
        } catch (e) {
          console.log('invalid magic code', e);
        }
      }
    }

    if (this.state.tfaSetup && this.state.tfaSetup.secret) {
      return this._template_setup2FA();
    }

    return this._template(title, magic, rawMagic);
  }

  _template(title, magic, rawMagic) {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });
    var instanceClass = classNames({
      'ui large input': true,
      'disabled': magic
    });

    return (
      <div>
        { magic && <div className="ui grid centered padded">
          <div className="ui info message">
            <div className="header">You have a magic code!</div>
            <ul className="list">
              <li>The log in form just turned into a signup form</li>
              <li>Your instance is <b>{magic.i}</b>,
              and your username is <b>{magic.u}</b></li>
            </ul>
          </div>
        </div> }

      <div className="ui basic segment very padded column centered grid">

        <div>
          <h2 className="ui header">{title} to your mnsig instance</h2>

          <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
            <input type="hidden" ref="magic" value={rawMagic}/>
            <div className="ui stack segment">

              <div className="field">
                <div className={instanceClass}>
                  <input type="text" ref="instance" name="instance" placeholder="Instance name"
                     pattern='.{3,}' title='3 characters minimum' required
                     autoComplete="off" autoCorrect="off" autoCapitalize="none" spellcheck="false"
                     defaultValue={magic && magic.i} readOnly={!!magic} />
                </div>
              </div>

              <div className="field">
                <div className={instanceClass}>
                  <input type="text" ref="username" name="username" placeholder="Username"
                      pattern='.{3,}' title='3 characters minimum' required
                      autoComplete="off" autoCorrect="off" autoCapitalize="none" spellcheck="false"
                      defaultValue={magic && magic.u} readOnly={!!magic} />
                </div>
              </div>

              <div className="field">
                <div className="ui large input">
                  <input type="password" ref="password" name="password" placeholder="Password"
                      pattern='.{10,}' title='10 characters minimum' required/>
                </div>
              </div>

              <div className="field" style={{display: this.state.tfaRequired ? "block" : "none"}}>
                <label>Enter your 2FA code</label>
                <div className="ui large input">
                  <input type="text" ref="totp" name="totp" placeholder="2FA code"
                      pattern='.{6}' title='Enter 6 characters' autoFocus={this.state.tfaRequired}
                      autoComplete="off" autoCorrect="off" autoCapitalize="none" spellcheck="false"/>
                </div>
              </div>

              <button className="ui large fluid primary submit button">{title}</button>

              <div className="ui error message">
                <div className="header">{title} failed</div>
                <p>{this.state.error}</p>
              </div>
            </div>
          </form>

        </div>
      </div>

      </div>
    );
  } // _template

  _template_setup2FA() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });

    return (
      <div>

        <div className="ui grid centered padded">
          <div className="ui info large message">
            <div className="header">Steps</div>
            <ol className="list">
              <li>If you miss the device that generates these
              tokens, enter in contact with the following <i>RESET
              CODE</i>: <b>{this.state.tfaSetup.reset}</b></li>
              <li>Scan the QR code with your TOTP application and
              enter the current token to complete your account setup.</li>
            </ol>
          </div>
        </div>

        <div className="ui basic segment very padded column centered grid">
          <div>
            <h2 className="ui header">Setup your 2FA</h2>

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <input type="hidden" ref="instance" value={this.formLogin.instance}/>
              <input type="hidden" ref="username" value={this.formLogin.username}/>
              <input type="hidden" ref="password" value={this.formLogin.password}/>

              <div className="ui stack segment">

                <div className="field">
                  <p>
                    <a href="#" onClick={this.onClickSecret.bind(this)}>
                    Click to {this.state.secretDisplayed ? "hide": "see"} your TOTP secret</a>
                  </p>
                  <QRCode value={this.state.tfaSetup.uri} size={256} />
                  <p ref="sharedsecret"></p>
                </div>

                <div className="field">
                  <div className="ui large input">
                    <input type="text" ref="totp" name="totp" placeholder="Current token"
                        pattern='.{6}' title='Enter 6 characters' required/>
                  </div>
                </div>

                <p>Proceed if you have saved your reset code.</p>
                <button className="ui large fluid primary submit button">Proceed</button>

                <div className="ui error message">
                  <div className="header">Setup failed</div>
                  <p>{this.state.error}</p>
                </div>
              </div>
            </form>
          </div>
        </div>

      </div>
    );
  } // _template_setup2FA

}

Login.contextTypes = {
  router: routerShape.isRequired
};


export default Login;
