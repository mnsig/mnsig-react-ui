'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape } from 'react-router';
import { generateKey } from 'mnsig-js-client';
import { decrypt } from 'sjcl';

import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


class WalletKeyAdd extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      waiting: false,
      error: null,

      localKey: false,
      waitingGen: false,
      errorGen: null
    };
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onUserChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var user = UserStore.getUser();
    if (!user.error || !user.token) {
      /* Added key or user token expired. */
      return this.context.router.replace('/');
    }
    this.setState({waiting: false, error: user.error});
  }

  onCancel(evt) {
    evt.preventDefault();
    this.context.router.replace('/');
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.setState({waiting: true, error: null});
    var data = {
      'wallet': this.props.params.wallet,
      'xpub': this.refs.xpub.value,
      'encxpriv': this.refs.encPriv.value,
    };
    UserActions.addKey(UserStore.getUser(), data);
  }

  onGenerate(evt) {
    evt.preventDefault();
    var plainPwd = this.refs.keypwd.value;
    if (!plainPwd || plainPwd.length < 10) {
      return this.setState({errorGen: true});
    }

    this.setState({errorGen: false, waitingGen: true}, function() {
      // Generate a key now.
      var result = generateKey(
        plainPwd,
        this.props.params.derive.toUpperCase(),
        this.props.params.network
      );
      this.refs.encPriv.value = result.xprivEnc;
      this.refs.xpub.value = result.xpub;
      this.refs.master.textContent = '';
      this.setState({waitingGen: false, localKey: true});
    });
  }

  onToggleShowKey(evt) {
    evt.preventDefault();
    var showing = !!this.refs.master.textContent;

    if (showing) {
      this.refs.master.textContent = '';
    } else {
      this.refs.master.textContent = decrypt(
        this.refs.keypwd.value,
        this.refs.encPriv.value
      );
    }
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });
    var formGenClass = classNames({
      'ui form': true,
      'loading': this.state.waitingGen
    });
    var pwdClass = classNames({
      'field': true,
      'error': this.state.errorGen
    });
    var showMasterStyle = {display: this.state.localKey ? 'block' : 'none'};

    var xpubInfo;
    if (this.props.params.derive.toUpperCase() === 'BIP44') {
      xpubInfo = "m/44'/network'/0'";
    } else {
      xpubInfo = "m";
    }

    return (
      <div className="ui text container">
        <h2 className="ui header">
          <div className="content">
            You're adding a key to <i>{this.props.params.wallet}</i>
          </div>
          <div className="sub header">
            Derivation strategy: {this.props.params.derive.toUpperCase()} &nbsp;&nbsp;
            Network: {this.props.params.network}
          </div>
        </h2>

        <div className="ui grid very padded">
          <div className="column">

            <div className="ui small info message">
              <div className="ui small header">Notice</div>
              <p>If this key will be used exclusively via API,
                submit this data using an API token instead.</p>
            </div>

            <h3 className="ui horizontal divider header">Generate a key locally</h3>
            <p>If you don't have a keypair already,
              generate one now and encrypt it locally using the password
              specified. The form below this one will be filled automatically.</p>
            <div className={formGenClass}>
              <div className="inline fields">
                <label>Key password</label>
                <div className={pwdClass}>
                  <input type="password" ref="keypwd" />
                </div>
                <a className="ui button small black" onClick={this.onGenerate.bind(this)}>
                  Generate key
                </a>
              </div>
              <p>Minimum password length: 10</p>

              <div style={showMasterStyle}>
                <button className="ui right labeled icon button"
                   onClick={this.onToggleShowKey.bind(this)}>
                  <i className="right icon unhide"></i>
                  Obtained master xpriv
                </button>
                <p style={{wordBreak: 'break-all'}} ref="master"></p>
              </div>

            </div>

            <div className="ui horizontal divider">Or</div>

            <h3 className="ui header">
              <div className="content">Import key</div>
              <div className="sub header">If you have generated a keypair already,
                copy and paste it below. Note that imported keys cannot be used to
                sign transactions using this website since it's not possible to
                decrypt its contents on the client.</div>
            </h3>
            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <div className="field">
                <label>Encrypted xpriv (optional)</label>
                <textarea ref="encPriv" rows="4"/>
              </div>

              <div className="field">
                <label>xpub ({xpubInfo})</label>
                <input type="text" ref="xpub" name="xpub" required autoComplete="off"/>
              </div>

              <div className="ui divider"></div>

              <button className="ui primary submit button">Add key</button>
              <button className="ui button" onClick={this.onCancel.bind(this)}>Cancel</button>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

WalletKeyAdd.contextTypes = {
  router: routerShape.isRequired
};


export default WalletKeyAdd;
