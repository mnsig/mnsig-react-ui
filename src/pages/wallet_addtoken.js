'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape, browserHistory } from 'react-router';

import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


class WalletAddToken extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      waiting: false,
      error: null,
      gotToken: null
    };
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onUserChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var user = UserStore.getUser();
    var token = UserStore.getLastAPIToken();
    if (!user.token) {
      /* User token expired. */
      return this.context.router.replace('/');
    }
    this.setState({waiting: false, error: user.error, gotToken: token});
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.setState({waiting: true, error: null});
    var ips = this.refs.restrictTo.value;
    var data = {
      'restrictIP': ips.replace(/ /g, '').split(','),
      'maxAmount': this.refs.maxTxAmount.value,
      'label': this.refs.tlabel.value
    };
    console.log('sending data', data, this.props.params.wallet);
    UserActions.addAPIToken(UserStore.getUser(), this.props.params.wallet, data);
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });
    var displayCurrency = this.props.params.currency;

    if (this.state.gotToken && this.state.gotToken.token) {
      var tok = this.state.gotToken;
      return (
        <div className="ui text container">
          <div className="ui message">
            <div className="ui header">Token created</div>

            <p>Token <b>{tok.label}</b> is ready to be used. The following
            code must be copy &amp; pasted now as you won't be seeing it again.</p>
            <p>Token: <code>{tok.token}</code></p>
            <br/>

            <button className="ui button" onClick={browserHistory.goBack}>
              Ok, take me home
            </button>
          </div>
        </div>
      );
    }

    return (
      <div className="ui text container">
        <h2 className="ui header">
          <div className="content">
            You're adding an API Token to <i>{this.props.params.wallet}</i>
          </div>
        </h2>

        <div className="ui grid very padded">
          <div className="sixteen wide tablet ten wide computer column">

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <div className="field">
                <label>Token label</label>
                <input type="text" ref="tlabel" name="tlabel" required autoComplete="off"
                  placeholder="How are you going to call this token?" />
              </div>

              <div className="field">
                <label>Restrict its use to the following IP addresses</label>
                <input type="text" ref="restrictTo" name="restrictTo" required autoComplete="off"
                  placeholder="Specify one or more addresses separated by comma"/>
              </div>

              <div className="field">
                <label>Maximum amount per transaction in {displayCurrency} (optional)</label>
                <input type="number" ref="maxTxAmount" name="maxTxAmount" step="any" autoComplete="off" />
                <p>Setting this to "5.0" causes the server to reject signatures coming from
                this API token for withdrawals for 5.0 {displayCurrency} or more. The
                token can still be used to create proposals, but can't send
                signed updates.</p>
              </div>

              <br/>
              <button className="ui primary submit button">Create token</button>
              <button className="ui button" onClick={browserHistory.goBack}>Cancel</button>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

WalletAddToken.contextTypes = {
  router: routerShape.isRequired
};


export default WalletAddToken;
