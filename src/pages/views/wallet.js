'use strict';

import { format } from 'util';
import React from 'react';
import Link from 'react-router/lib/Link';
import UserStore from '../../stores/UserStore';
import UserActions from '../../actions/UserActions';
import WalletCurrency from '../../constants/WalletCurrency';


class Wallet extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      user: UserStore.getUser(),
      data: UserStore.getData(),
      creatingAddress: null,
      deletingToken: null,
      deletedToken: null
    };
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onUserChange);
    if (this.state.user.token) {
      UserActions.getUserData(this.state.user);
    }
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var data = UserStore.getData();
    var newState = {
      user: UserStore.getUser(),
      data: data
    };

    if (this.state.creatingAddress) {
      // Check if a new address was created.
      var walletName = this.state.creatingAddress;
      var freshAddress = data.lastAddress;
      if (freshAddress && freshAddress.wallet === walletName) {
        newState.creatingAddress = null;
      }
    }

    this.setState(newState);

    if (this.state.deletingToken) {
      // Check if an API token got deleted.
      var walletRow = this.state.deletingToken;
      var removedToken = data.removedToken;
      if (removedToken && walletRow.endsWith('_' + removedToken.wallet)) {
        var deleted = this.state.deletingToken;
        this.setState({deletingToken: null, deletedToken: deleted}, function() {
          // Refresh data.
          UserActions.getUserData(this.state.user);
        });
      }
    }
  }

  _onAddressUpdated(evt) {
    console.log(evt, 'address updated');
  }

  onCreateAddress(walletName, evt) {
    evt.preventDefault();
    this.setState({creatingAddress: walletName});
    UserActions.createAddress(this.state.user, walletName);
  }

  onDeleteAPIToken(walletName, tokenId, rowKey, evt) {
    evt.preventDefault();
    this.setState({deletingToken: rowKey + '_' + walletName});
    UserActions.deleteAPIToken(this.state.user, walletName,
                               {tokenId: tokenId});
  }

  render() {
    if (!this.state.data) {
      return null;
    }

    var self = this;
    var wallet;
    var walletName = this.props.params.wallet;
    var current_user = this.state.user.username;
    var freshAddress = this.state.data.lastAddress;
    var removedToken = this.state.data.removedToken;

    for (var i = 0; i < this.state.data.wallets.length; i++) {
      if (this.state.data.wallets[i].name == walletName) {
        wallet = this.state.data.wallets[i];
        break;
      }
    }

    if (!wallet) {
      return null;
    }

    var isAdmin = false;
    var canSign = false;
    var canDerive = false;
    var subHeader = format(
      "%d-of-%d / %s / %s",
      wallet.m, wallet.n, wallet.derivation ? wallet.derivation.toUpperCase() : "normal",
      wallet.type
    );
    var needConfig = false;
    var info = null;
    var addressAndBalance = null;
    var users = [];
    var apiTokens = [];
    var proposals = [];
    var walletReady = false;
    var currency = WalletCurrency[wallet.type];

    wallet.users.forEach(function(user, i) {
      var username = user.username;
      var permissions = user.permissions.join(', ');
      if (current_user === username && permissions.indexOf('admin') >= 0) {
        isAdmin = true;
        canSign = true;
        canDerive = true;
      }
      if (permissions.indexOf('sign') >= 0) {
        canSign = true;
      }
      if (permissions.indexOf('derive') >= 0) {
        canDerive = true;
      }
      users.push(
        <tr key={format("user-%d", i)}>
          <td>{username}</td>
          <td>{permissions}</td>
        </tr>
      );
    });

    wallet.api_tokens.forEach(function(tok, i) {
      var remove = null;
      var keyRow = format("token-%d", i);
      if (self.state.deletedToken && self.state.deletedToken.startsWith(keyRow + '_')) {
        return null;
      }
      var deleting = self.state.deletingToken ? self.state.deletingToken.startsWith(keyRow + '_') : false;
      if (isAdmin) {
        remove = (
          <button className={"ui button icon basic mini red" + (deleting ? " loading" : "")}
              style={{marginBottom: '-4px'}}
              onClick={self.onDeleteAPIToken.bind(self, wallet.name, tok.id, keyRow)}
              title="Remove this token">
            <i className="icon remove"></i>
          </button>
        );
      }

      apiTokens.push(
        <tr key={keyRow} className={deleting ? "negative disabled" : ""}>
          <td>{remove} &nbsp;&nbsp; {tok.label}</td>
          <td>{tok.max_txamount ?
                  format("%s %s", tok.max_txamount / 1e8, currency) :
                  "Disabled"}</td>
        </tr>
      );
    });

    wallet.pending_proposals.forEach(function(proposal, i) {
      if (proposal.state === 'unprepared') {
        return;
      }

      var keyRow = format('prop-%d', i);
      var link = format(
        "/proposal/%s/%s/%d", wallet.name,
        wallet.derivation ? wallet.derivation.toUpperCase() : "normal",
        proposal.id);
      var style = {textDecoration: 'none'};

      proposals.push(
        <tr key={keyRow} className={proposal.state === 'complete' ? 'positive': ''}>
          <td className="selectable">
            <Link to={link} style={style}>{proposal.id}</Link>
          </td>
          <td className="selectable">
            <Link to={link} style={style}>{proposal.dest_address}</Link>
          </td>
          <td className="selectable">
            <Link to={link} style={style}>{proposal.amount / 1e8} {currency}</Link>
          </td>
          <td className="selectable">
            <Link to={link} style={style}>{proposal.external_id}</Link>
          </td>
        </tr>
      );
    });

    if (wallet.complete && !wallet.configured) {
      needConfig = true;
    }

    if (!wallet.complete) {
      info = format(
        "This wallet is not complete yet, missing keys: %d",
        wallet.n - wallet.num_keys);
      info = (
        <div className="ui warning message small">
          <div className="header">Wallet not complete</div>
          <p>{info}</p>
          {canSign &&
            <Link to={format("/key/%s/%s/%s", wallet.name, wallet.derivation, wallet.type)}
               className="ui small primary button">Add key</Link>}
        </div>
      );
    } else if (needConfig) {
      info = (
        <div className="ui info message small">
          <div className="header">Manual step required</div>
          <p>This wallet is complete but requires an internal adjust
          before you can use it.<br/>Contact support.</p>
        </div>
      );
    }

    if (wallet.balance) {
      walletReady = true;
      var balance = (
        <table className="ui table small right aligned">
          <thead>
            <tr><th colSpan="2" className="center aligned">Wallet Balance</th></tr>
          </thead>
          <tbody>
            <tr>
              <td><p><b>Confirmed</b></p></td><td><p>{wallet.balance.confirmed} {currency}</p></td>
            </tr>
            <tr>
              <td><p><b>Total</b></p></td><td><p>{wallet.balance.total} {currency}</p></td>
            </tr>
          </tbody>
        </table>
      );

      var lastAddress = wallet.last_address.address;
      if (freshAddress && freshAddress.wallet === wallet.name) {
        lastAddress = freshAddress.address;
      }

      var addresses = (
        <table className="ui table small left aligned">
          <thead>
            <tr>
              <th colSpan="2">
                Last address
                {canDerive ?
                  <button title="Create a new address"
                      style={{height: '26px', marginBottom: '-6px'}}
                      className={"ui right floated mini green button" + (this.state.creatingAddress ? " loading": "")}
                      onClick={this.onCreateAddress.bind(this, wallet.name)}>
                    ADD
                  </button> :
                  null
                }
              </th>
            </tr>
          </thead>
          <tbody>
            <tr><td>
              <p style={{wordBreak: 'break-all'}}>
                {lastAddress || String.fromCharCode(8212)}
              </p>
            </td></tr>
          </tbody>
        </table>
      );

      addressAndBalance = (
        <div className="ui grid">
          <div className="sixteen wide tablet nine wide computer column">
            {addresses}
          </div>
          <div className="sixteen wide table seven wide computer column">
            {balance}
          </div>
        </div>
      );
    }

    return (
      <div className="ui text container">
      <div className="ui basic segment">

        <h3 className={"ui top attached clearing header inverted" + (needConfig ? " yellow" : '')}>
          <div className="content">
            {wallet.name}
            <div className="sub header">{subHeader}</div>
          </div>
          { walletReady ?
            <Link to={format("/history/%s", wallet.name)}
                className="ui right floated small inverted button">History</Link> :
            null }
        </h3>

        <div className="ui attached segment">

          {info}

          {addressAndBalance}

          <h4>
            Pending proposals
            {canDerive && walletReady &&
            <Link to={format("/proposal/%s", wallet.name)}
               className="ui right floated secondary tiny button">
              Start withdrawal
            </Link>}
          </h4>
          {this._table(['ID', 'Destination', 'Amount', 'Ext ID'], proposals, 'selectable')}
          {proposals.length ?
            <p className="ui tiny">Click in a row to see a proposal, maybe sign, broadcast, or cancel it.</p> :
            null}

          <div className="ui divider"></div>

          <h4>
            API Tokens
            {isAdmin &&
            <Link to={format("/apitoken/%s/%s", wallet.name, currency)}
               className="ui right floated secondary tiny button">
              Add token
            </Link>}
          </h4>
          {this._table(['Token', 'Transaction limit'], apiTokens)}

          <h4>
            Users
            {isAdmin &&
            <Link to={format("/user/%s", wallet.name)}
               className="ui right floated secondary tiny button">
              Add user
            </Link>}
          </h4>
          {this._table(['Username', 'Permissions'], users, 'fixed')}

        </div>

      </div></div>
    );
  }

  _table(columns, body, tableClass = "") {
    var headers = columns.map(function(col) {
      return <th key={"header-" + col}>{col}</th>;
    });
    return (
      <table className={'ui small ' + tableClass + ' table'}>
        <thead>
          <tr>{headers}</tr>
        </thead>
        <tbody>{body}</tbody>
      </table>
    );
  }

}


export default Wallet;
