'use strict';

import React from 'react';


class NoUserHome extends React.Component {

  render() {
    return (
      <div>

        <div className="ui basic segment vertical center aligned padded">
          <div className="ui container">
            <div className="ui vertical stripe segment very padded">
              <img src="img/mnsig_logo.png" style={{width: '150px'}}/>
              <h3><code>The crypto signature service -- mnsig</code></h3>
            </div>
          </div>
        </div>

        <div className="ui text container basic segment center aligned" style={{paddingTop: 0, marginTop: 0}}>
          <h3 className="ui header">What is this?</h3>
          <p>mnsig is a <a href="https://bitcoin.org/en/glossary/p2sh-multisig" target="external" rel="noreferrer">P2SH multisig</a> wallet +
          bitcoin-core with some sugar on top of it.</p>
          <p>You hold the keys, mnsig handles the rest.</p>
          <div className="ui horizontal list header aligned center">
            <span className="item">m-of-n isolated wallets</span>
            <span className="item">offline signing</span>
            <span className="item">unmetered API</span>
            <span className="item">manage your wallets by yourself or with a team</span>
          </div>
        </div>

        <div className="ui text container basic segment">
          <h3 className="ui header">How can I use it?</h3>

          <div className="ui two column stackable grid">
            <div className="column">
              <div className="ui horizontal basic segment grey">
                <p>You can manage, support, and deploy it on your own servers.<br/><br/>
                Visit <a href="https://gitlab.com/mnsig" target="external" rel="noreferrer">
                https://gitlab.com/mnsig</a> to read documentation, download the source code,
                and contribute to it.</p>
              </div>
            </div>

            <div className="column">
              <div className="ui horizontal basic segment center aligned blue">
                <p>Or you can let mnsig.com setup, manage, and secure deployments
                for you.<br/><br/>
                <a className="ui blue button disabled">Not public yet</a>
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="ui vertical footer segment">
          <div className="ui center aligned container">
            <p className="ui tertiary">Thank you for visiting mnsig.com</p>
          </div>
        </div>

      </div>
    );
  }
}


export default NoUserHome;
