'use strict';

import { format } from 'util';
import React from 'react';
import Link from 'react-router/lib/Link';
import WalletCurrency from '../constants/WalletCurrency';


class WalletSummary extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    var wallet = this.props.wallet;
    var needConfig;
    var subHeader = format(
        "%d-of-%d / %s / %s",
        wallet.m, wallet.n, wallet.derivation ? wallet.derivation.toUpperCase() : "normal",
        wallet.type
    );
    var currency = WalletCurrency[wallet.type];

    if (wallet.complete && !wallet.configured) {
      needConfig = true;
    } else {
      needConfig = false;
    }

    return (
      <div className="ui horizontal segments">

        <div className="ui segment">
          <h3 className={"ui header" + (needConfig ? " yellow" : '')}>
            <div className="content">
              <Link to={format("/wallet/%s", wallet.name)}>
                {wallet.name}
              </Link>
              <div className="sub header">{subHeader}</div>
            </div>
          </h3>
        </div>

        <div className="ui right aligned segment" style={{borderLeft: 0}}>
          <div className="ui sub header">
            <span>{wallet.balance && (wallet.balance.confirmed + " " + currency)}</span>
          </div>
        </div>

      </div>
    );
  }

}


export default WalletSummary;
