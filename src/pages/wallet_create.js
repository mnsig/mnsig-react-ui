'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape, browserHistory } from 'react-router';

import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


class WalletCreate extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      waiting: false,
      error: null
    };
  }

  componentDidMount() {
    this.refs.name.focus();
    UserStore.addChangeListener(this._onUserChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var user = UserStore.getUser();
    if (!user.error || !user.token) {
      /* Created wallet or token expired. */
      return this.context.router.replace('/');
    }
    this.setState({waiting: false, error: user.error});
  }

  onSubmit(evt) {
    evt.preventDefault();
    var data = {
      'wallet': this.refs.name.value,
      'type': this.refs.type.value,
      'm': this.refs.m.value,
      'n': this.refs.n.value,
      'derivation': 'bip44',
    };
    if (data['m'] > data['n']) {
      this.setState({error: 'm cannot be greater than n'});
      return;
    }
    this.setState({waiting: true, error: null});
    UserActions.createWallet(UserStore.getUser(), data);
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });

    return (
      <div className="ui text container">
        <h2 className="ui header">You're creating a new wallet</h2>
        <h5>The keys for the wallet will be added in a next step</h5>

        <div className="ui grid very padded">
          <div className="sixteen wide tablet six wide computer column">

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <div className="field">
                <label>Wallet name</label>
                <input type="text" ref="name" name="name" required
                  autoComplete="off"/>
              </div>

              <div className="field">
                <label>Wallet type</label>
                <select className="ui search dropdown" ref="type" name="type" required>
                    <option value="">Select a type</option>
                    <option value="bitcoin">Bitcoin</option>
                    <option value="testnet">Bitcoin Testnet</option>
                    <option value="dash">Dash</option>
                    <option value="dashtest">Dash Testnet</option>
                </select>
              </div>

              <div className="field">
                <label>Required signers (m)</label>
                <input type="number" ref="m" name="m" defaultValue="2" min="1" max="8"
                  required/>
              </div>

              <div className="field">
                <label>Total signers (n)</label>
                <input type="number" ref="n" name="n" defaultValue="4" min="1" max="8"
                  required/>
              </div>

              <button className="ui primary submit button">Create</button>
              <button className="ui button" onClick={browserHistory.goBack}>Cancel</button>

              <div className="ui error message">
                <div className="header">Failed to create wallet</div>
                <p>{this.state.error}</p>
              </div>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

WalletCreate.contextTypes = {
  router: routerShape.isRequired
};


export default WalletCreate;
