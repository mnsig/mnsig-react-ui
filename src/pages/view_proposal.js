'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape, browserHistory } from 'react-router';
import { decryptKey } from 'mnsig-js-client';

import UserStore from '../stores/UserStore';
import TxStore from '../stores/TxStore';
import TxActions from '../actions/TxActions';
import WalletStore from '../stores/WalletStore';
import WalletActions from '../actions/WalletActions';


class ViewProposal extends React.Component {

  constructor(props) {
    super(props);

    this._onTxChange = this._onTxChange.bind(this);
    this._onWalletChange = this._onWalletChange.bind(this);

    this.state = {
      waiting: false,
      error: null,
      proposal: null,
      keys: null,

      checkedKey: 0,
      recipients: null,
      note: '...'
    };
  }

  componentDidMount() {
    TxStore.addChangeListener(this._onTxChange);
    WalletStore.addChangeListener(this._onWalletChange);

    // Get proposal by id.
    var data = {
      'wallet': this.props.params.wallet,
      'id': this.props.params.id
    };
    TxActions.getProposal(UserStore.getUser(), data);
  }

  componentWillUnmount() {
    TxStore.removeChangeListener(this._onTxChange);
    WalletStore.removeChangeListener(this._onWalletChange);
  }

  _onTxChange() {
    var tx = TxStore.getTx();
    if (tx.userError) {
      // The user token expired.
      return this.context.router.replace('/');
    } else if (tx.data && tx.data.state === 'broadcasted') {
      // The transaction has been broadcasted.
      return this.context.router.goBack();
    }

    if (tx.errorCode) {
      return this.setState({waiting: false, error: tx.data.message});
    } else if (tx.data && tx.data.state === 'canceled') {
      // User canceled proposal.
      return this.context.router.goBack();
    } else {
      if (!this.state.keys) {
        // Fetch user keys.
        WalletActions.getKeys(UserStore.getUser(), this.props.params.wallet);
      }
      var newState = {waiting: false, error: null, proposal: tx.data};
      if (tx.data.recipients) {
        newState['recipients'] = Object.assign({}, tx.data.recipients);
        newState['note'] = tx.data.note || '';
      }
      this.setState(newState);
    }
  }

  _onWalletChange() {
    var keys = WalletStore.getKeys();
    this.setState({keys: keys.data.keys});
  }

  onCancelProposal(evt) {
    evt.preventDefault();
    this.setState({waiting: true, error: null});
    var data = {
      'wallet': this.props.params.wallet,
      'id': this.props.params.id
    };
    TxActions.cancelProposal(UserStore.getUser(), data);
  }

  onSubmit(evt) {
    evt.preventDefault();
    if (this.state.proposal.state === 'complete') {
      return this._broadcast();
    }

    // Sign.
    var pwd = this.refs.keypwd.value;
    var key = this.state.keys[this.state.checkedKey];
    var network = this.state.proposal.wallet_type;
    var xpriv = decryptKey(pwd, key.encryped_xpriv, this.props.params.derive, network);
    if (xpriv === null) {
      return this.setState({error: 'Check your password'});
    }

    this.setState({waiting: true, error: null, proposal: null});
    TxActions.sendProposalSig(UserStore.getUser(), this.state.proposal, xpriv);
  }

  _onChangeKey(evt) {
    var target = evt.currentTarget;
    this.setState({checkedKey: target.value});
  }

  _broadcast() {
    console.log(this.state.proposal);
    this.setState({waiting: true, error: null});
    TxActions.broadcastProposal(UserStore.getUser(), this.state.proposal);
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': !this.state.error && (this.state.waiting || !this.state.proposal || !this.state.keys),
      'error': this.state.error
    });
    var p = this.state.proposal;
    var txfee = null;
    var extid = null;
    var signWith = null;
    var recipients = null;
    var signers = {};

    (p && p.xpub_signed || []).forEach(function(xpub) {
      signers[xpub] = true;
    })

    if (this.state.keys && p && p.state !== 'complete') {
      var self = this;
      var xpubs = (this.state.keys || []).map(function(key, i) {
        var last20 = key.xpub.substr(key.xpub.length - 20);
        var alreadySigned = signers[key.xpub];
        return (
          <div className="field" key={key.xpub}>
            <div className="ui radio checkbox">
              <input type="radio" name="signwith"
                disabled={alreadySigned}
                defaultChecked={!i && !alreadySigned ? true : false}
                defaultValue={i} onChange={self._onChangeKey.bind(self)}/>
              <label>xpub ... {last20} {alreadySigned && <i className="large green checkmark icon"></i>}</label>
            </div>
          </div>
        );
      });
      signWith = (
        <div>
          <div className="grouped fields">
            <label>Sign with</label>
            {xpubs}
          </div>

          <div className="field">
            <label>Key password</label>
            <input type="password" ref="keypwd" />
          </div>
        </div>
      );
    }

    if (p) {
      txfee = p.fee_satoshis / 1e8;
      extid = p.external_id;
    }

    recipients = Object.keys(this.state.recipients || {}).map(function(addy, indx) {
      var amount = this.state.recipients[addy] / 1e8;
      return (
        <div className="fields" key={"recipient-" + indx}>
          <div className="twelve wide field">
            {indx === 0 && <label>Address</label>}
            <input type="text" value={addy} readOnly/>
          </div>
          <div className="four wide field">
            {indx === 0 && <label>Amount</label>}
            <input type="text" value={amount} readOnly/>
          </div>
        </div>
      );
    }.bind(this));

    return (
      <div className="ui text container">
        <h2 className="ui header">You're viewing a proposal for <i>{this.props.params.wallet}</i></h2>

        <div className="ui grid very padded">
          <div className="sixteen wide tablet twelve wide computer column">

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <h5 className="ui header">Recipient</h5>
              {recipients}

              <div className="field">
                <label>Network fee</label>
                <input type="text" name="fee" value={txfee} readOnly/>
              </div>

              <div className="field">
                <label>Note (optional)</label>
                <input type="text" name="note" value={this.state.note} readOnly/>
              </div>

              <div className="field">
                <label>External ID (optional)</label>
                <input type="text" name="extid" value={extid} readOnly/>
              </div>

              {signWith}

              <br/>
              {signWith && <button className="ui primary submit button">Sign</button>}
              {p && p.state === 'complete' && <button className="ui primary submit button">Broadcast</button>}
              <button className="ui button" onClick={browserHistory.goBack}>Cancel</button>
              {p && p.state !== 'complete' && <button className="ui right floated red button"
                onClick={this.onCancelProposal.bind(this)}>Cancel proposal</button>}

              <div className="ui error message">
                <div className="header">Failed to update proposal</div>
                <p>{this.state.error}</p>
              </div>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

ViewProposal.contextTypes = {
  router: routerShape.isRequired
};


export default ViewProposal;
