'use strict';

import crypto from 'crypto';
import React from 'react';
import classNames from 'classnames';


class RawPassword extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      waiting: false,
      result: '',
    };
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.setState({waiting: true}, function() {
      var rawpw = this.refs.password.value;
      this.setState({
        result: crypto.createHash('sha256').update(rawpw).digest('hex'),
        waiting: false
      });
    });
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
    });

    return (
      <div className="ui basic segment centered grid">

        <div>
          <h2 className="ui header">Raw</h2>

          <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
            <div className="ui stack segment">

              <div className="field">
                <div className="ui input">
                  <input type="password" ref="password" name="password" placeholder="Password" autoComplete="off" pattern='.{10,}' title='10 characters minimum' required/>
                </div>
              </div>
              <button className="ui fluid primary submit button">Generate raw</button>

              <br/>
              <div className="field">
                <label>Raw result</label>
                <div className="ui input">
                  <textarea style={{fontFamily: 'monospace'}} rows="4" value={this.state.result} />
                </div>
              </div>

            </div>
          </form>
        </div>

      </div>
    );
  } // render

}


export default RawPassword;
