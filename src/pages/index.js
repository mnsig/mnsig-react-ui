'use strict';

import { format } from 'util';
import React from 'react';
import Link from 'react-router/lib/Link';
import classNames from 'classnames';
import UserStore from '../stores/UserStore';


class Index extends React.Component {
  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      user: UserStore.getUser(),
      data: UserStore.getData()
    };
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onUserChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    this.setState({
      user: UserStore.getUser(),
      data: UserStore.getData()
    });
  }

  render() {
    /* Build page header. */

    var logo;
    var logoElem;
    var logoStyle = {padding: '6px 0 0 0'};
    var menu;

    if (this.state.user.token) {
      /* User logged in. */
      logo = '/img/mnsig_small_logo_white_rect.png';
    } else {
      /* No user present. */
      logo = '/img/mnsig_small_logo_rect.png';
    }

    logoElem = <img className="logo" src={logo} alt="mnsig.com" title="mnsig.com" style={{width: '48px'}}/>;

    if (this.state.user.token) {
      var text = format('Logout %s @ %s', this.state.user.username, this.state.user.instance);
      menu = (
        <div>
          <div className="ui fixed inverted menu">
            <div className="ui container">
              <Link to="/" className="header item">{logoElem}</Link>
              <div className="ui simple dropdown item">
                Wallets
                <i className="dropdown icon"></i>
                <div className="menu">
                  <Link to="/" className="item">Show them</Link>
                  <div className="divider"></div>
                  <Link to="/create" className="item">Create</Link>
                </div>
              </div>
              <Link to="/logout" className="right item">{text}</Link>
            </div>
          </div>
          {/* Add spacing from the fixed menu */}
          <div style={{paddingTop: '7rem'}}></div>
        </div>
      );
    } else {
      menu = (
        <div className="ui basic segment vertical">
          <div className="ui container">
            <div className="ui secondary menu">
              <Link to="/" style={logoStyle}>{logoElem}</Link>
              <div className="right item">
                <Link to="/login" className="ui black button">Log in</Link>
              </div>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        {menu}
        {this.props.children}
      </div>
    );
  }
}


export default Index;
