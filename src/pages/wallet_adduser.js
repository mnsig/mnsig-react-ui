'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape, browserHistory } from 'react-router';

import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';


class WalletAddUser extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      waiting: false,
      error: null
    };
  }

  componentDidMount() {
    this.refs.username.focus();
    UserStore.addChangeListener(this._onUserChange);
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    var user = UserStore.getUser();
    if (!user.error || !user.token) {
      /* User got added or token expired. */
      return this.context.router.replace('/');
    }
    this.setState({waiting: false, error: user.error});
  }

  onSubmit(evt) {
    evt.preventDefault();
    this.setState({waiting: true, error: null});

    var self = this;
    var permissions = [];
    ['derive', 'sign', 'view'].forEach(function(p) {
      if (self.refs['perm-' + p].checked) {
        permissions.push(p);
      }
    });
    var data = {
      'username': this.refs.username.value,
      'perms': permissions
    };
    UserActions.addUserWallet(UserStore.getUser(), this.props.params.wallet, data);
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });

    return (
      <div className="ui text container">
        <h2 className="ui header">You're adding an user to <i>{this.props.params.wallet}</i></h2>

        <div className="ui grid very padded">
          <div className="column">

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>

              <div className="fields">
              <div className="field">
                <label>Username</label>
                <input type="text" ref="username" name="username" required
                  autoComplete="off"/>
              </div>
              </div>

              <div className="grouped fields">
                <label>Permissions</label>
                <div className="field">
                  <div className="ui checkbox">
                    <input type="checkbox" defaultChecked={true} ref="perm-view"/>
                    <label><b>View</b><br/>
                    Can this user see information about this wallet?
                    It's very likely you want this permission.</label>
                  </div>
                </div>
                <div className="field">
                  <div className="ui checkbox">
                    <input type="checkbox" ref="perm-derive"/>
                    <label><b>Derive</b><br/>
                    Can this user get new addresses and submit proposals?
                    </label>
                  </div>
                </div>
                <div className="field">
                  <div className="ui checkbox">
                    <input type="checkbox" ref="perm-sign"/>
                    <label><b>Sign</b><br/>
                    Can this user submit signed proposals? If you're
                      setting this, also let this new user add a key to this wallet so it's
                      possible to sign using this website.
                    </label>
                  </div>
                </div>
              </div>
              <br/>

              <button className="ui primary submit button">Add user</button>
              <button className="ui button" onClick={browserHistory.goBack}>Cancel</button>

              <div className="ui error message">
                <div className="header">Failed to add user</div>
                <p>{this.state.error}</p>
              </div>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

WalletAddUser.contextTypes = {
  router: routerShape.isRequired
};


export default WalletAddUser;
