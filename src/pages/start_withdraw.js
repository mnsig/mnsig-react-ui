'use strict';

import React from 'react';
import classNames from 'classnames';
import { routerShape, browserHistory } from 'react-router';

import UserStore from '../stores/UserStore';
import TxStore from '../stores/TxStore';
import TxActions from '../actions/TxActions';


class CreateRawProposal extends React.Component {

  constructor(props) {
    super(props);

    this._onTxChange = this._onTxChange.bind(this);

    this.state = {
      waiting: false,
      error: null
    };
  }

  componentDidMount() {
    this.refs.destination.focus();
    TxStore.addChangeListener(this._onTxChange);
  }

  componentWillUnmount() {
    TxStore.removeChangeListener(this._onTxChange);
  }

  _onTxChange() {
    var tx = TxStore.getTx();
    if (tx.userError) {
      // It's likely that the user token expired.
      return this.context.router.replace('/');
    }
    if (tx.errorCode) {
      return this.setState({waiting: false, error: tx.data.message});
    }

    if (tx.data.state === 'unprepared') {
      // Proposal created, prepare it.
      TxActions.prepareProposal(UserStore.getUser(), tx.data);
      return;
    }

    if (tx.data.updated && tx.data.proposal.state === 'no_signatures') {
      // Proposal is ready to be signed.
      return this.context.router.goBack();
    }
  }

  onSubmit(evt) {
    evt.preventDefault();
    var data = {
      'wallet': this.props.params.wallet,
      'dest': this.refs.destination.value,
      'amount': this.refs.amount.value,
      'note': this.refs.note.value
    };
    this.setState({waiting: true, error: null});
    TxActions.startProposal(UserStore.getUser(), data);
  }

  render() {
    var formClass = classNames({
      'ui form': true,
      'loading': this.state.waiting,
      'error': this.state.error
    });

    return (
      <div className="ui text container">
        <h2 className="ui header">You're starting a new withdraw for <i>{this.props.params.wallet}</i></h2>
        <h5>No signing will happen at this step</h5>

        <div className="ui grid very padded">
          <div className="sixteen wide tablet nine wide computer column">

            <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
              <div className="field">
                <label>Recipient address</label>
                <input type="text" ref="destination" name="destination" required
                  placeholder="Which address will receive this?"
                  autoComplete="off"/>
              </div>

              <div className="field">
                <label>Amount</label>
                <input type="number" ref="amount" name="amount" step="any" required/>
              </div>

              <div className="field">
                <label>Note (optional)</label>
                <input type="text" ref="note" name="note"
                  placeholder="Custom note, it's up to you"/>
              </div>
              <br/>

              <button className="ui primary submit button">Create</button>
              <button className="ui button" onClick={browserHistory.goBack}>Cancel</button>

              <div className="ui error message">
                <div className="header">Failed to create proposal</div>
                <p>{this.state.error}</p>
              </div>
            </form>

          </div>
        </div>

      </div>
    );
  }
}

CreateRawProposal.contextTypes = {
  router: routerShape.isRequired
};


export default CreateRawProposal;
