'use strict';

import { format } from 'util';
import React from 'react';
import Link from 'react-router/lib/Link';
import UserStore from '../stores/UserStore';
import UserActions from '../actions/UserActions';
import NoUserHome from './views/nouser_home';
import WalletSummary from './wallet_summary';
import { version } from '../../package.json';


var versionStyle = {
  position: "fixed",
  left: "1rem",
  bottom: "0.5rem",
  color: "white",
  "zIndex": 100,
  "textShadow": "1px 1px black",
};

class Dashboard extends React.Component {

  constructor(props) {
    super(props);

    this._onUserChange = this._onUserChange.bind(this);

    this.state = {
      user: UserStore.getUser(),
      data: UserStore.getData()
    };
  }

  componentDidMount() {
    UserStore.addChangeListener(this._onUserChange);
    if (this.state.user.token) {
      UserActions.getUserData(this.state.user);
    }
  }

  componentWillUnmount() {
   UserStore.removeChangeListener(this._onUserChange);
  }

  _onUserChange() {
    this.setState({
      user: UserStore.getUser(),
      data: UserStore.getData()
    });
  }

  render() {
    if (!this.state.user.token) {
      return <NoUserHome />;
    }

    if (this.state.data === null) {
      return null;
    }

    var wallets;
    if (!this.state.data.wallets || !this.state.data.wallets.length) {
      wallets = (
        <div className="ui basic segment">
          <h4 className="ui header">... That's something you don't have</h4>
          <Link to="/create" className="ui button primary">Create a wallet</Link>
        </div>
      );
    } else {
      wallets = this.state.data.wallets.map(function(wallet, indx) {
        return <WalletSummary key={'wallet-' + indx} wallet={wallet}/>;
      });
    }

    return (
      <div className="ui text container">
        <div style={versionStyle}>mnsig c-{version}</div>
        <h2 className="ui header">Wallets</h2>
        <div className="ui segments">
          {wallets}
        </div>
      </div>
    );
  }

}


export default Dashboard;
