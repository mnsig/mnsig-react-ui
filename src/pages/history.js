'use strict';

import React from 'react';
import classNames from 'classnames';
import Link from 'react-router/lib/Link';
import { routerShape, browserHistory } from 'react-router';
import { format } from 'util';

import UserStore from '../stores/UserStore';
import WalletStore from '../stores/WalletStore';
import WalletActions from '../actions/WalletActions';
import WalletCurrency from '../constants/WalletCurrency';


class WalletHistory extends React.Component {

  constructor(props) {
    super(props);

    this._onWalletChange = this._onWalletChange.bind(this);

    this.state = {
      error: null,
      loading: true,
      data: null,
      exporting: false
    };
  }

  componentDidMount() {
    WalletStore.addChangeListener(this._onWalletChange);
    // Get history.
    this._getTransactions(this.props.params.page);
  }

  componentWillUnmount() {
    WalletStore.removeChangeListener(this._onWalletChange);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.params.page != this.props.params.page) {
      this._getTransactions(nextProps.params.page);
    }
  }

  _getTransactions(page) {
    var param = {page: page};
    WalletActions.getTransactions(
      UserStore.getUser(), this.props.params.wallet, param, null
    );
  }

  _onWalletChange() {
    var txdata = WalletStore.getTransactions();
    var exportdata = WalletStore.getExport();
    if (txdata.userError || exportdata.userError) {
      WalletStore.reset();
      return this.context.router.replace('/');
    }
    if (txdata.data !== null) {
      this.setState({loading: false, data: txdata.data});
    }
    if (this.state.exporting && exportdata.data !== null) {
      this._export(exportdata.data.csv);
      this.setState({exporting: false});
    }
  }

  _export(csvList) {
    var csv = csvList.map(function(row) {
      return row.join(',');
    }).join('\r\n');
    var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    var btn = this.refs.csvexport;
    btn.href = csvData;
    btn.download = 'export_' + (new Date().getTime()) + '.csv';
    btn.click();
    btn.download = null;
    btn.href = null;
  }

  onSubmit(evt) {
    evt.preventDefault();
  }

  onExport(evt) {
    evt.preventDefault();
    WalletActions.exportTransactions(UserStore.getUser(), this.props.params.wallet);
    this.setState({exporting: true});
  }

  render() {
    var txlist = null;
    var pagination = null;
    var formClass = classNames({
      'ui form': true,
      'error': this.state.error
    });
    var currency = null;

    if (this.state.loading) {
      txlist = (
        <tr className="ui segment">
          <td colSpan="6" className="ui active">Loading</td>
        </tr>);
    } else if (this.state.data) {
      currency = WalletCurrency[this.state.data.wallet_type];
      var breakWord = {wordBreak: 'break-all'};
      var bestheight = this.state.data.bestblock_height || 0;
      txlist = this.state.data.data.map(function(entry, indx) {
        var confirmations = 0;
        if (entry.blockheight) {
          confirmations = bestheight - entry.blockheight + 1;
        }
        return (
          <tr key={"row-" + indx} className={confirmations >= 2 ? null : "warning"}>
            <td>{entry.complete_date.split('T').join(' ')}</td>
            <td style={breakWord}>{entry.txid}</td>
            <td style={breakWord}>{entry.dest_address}</td>
            <td>{entry.amount / 1e8}</td>
            <td>{entry.note}</td>
            <td className="center aligned">{confirmations}</td>
          </tr>);
      });

      if (this.state.data.next_prev) {
        var nextPage = this.state.data.next_prev[0];
        var prevPage = this.state.data.next_prev[1];
        pagination = (
          <div style={{display: "inline"}}>
            <Link to={format("/history/%s/%s", this.props.params.wallet, prevPage)}
                className={"ui button secondary " + (prevPage ? null : "basic disabled")}>
              Prev page
            </Link>
            <Link to={format("/history/%s/%s", this.props.params.wallet, nextPage)}
                className={"ui button secondary " + (nextPage ? null : "basic disabled")}>
              Next page
            </Link>
          </div>
        );
      }
    }

    return (
      <div className="ui basic segment container" style={{paddingTop: 0, marginTop: 0}}>
        <h2 className="ui header"><i>{this.props.params.wallet}</i>'s transaction history</h2>

        {/*<div className="ui">
          <p>Filter by</p>
          <form className={formClass} onSubmit={this.onSubmit.bind(this)}>
            <div className="field">
              <label>Address</label>
              <input type="text" ref="address" />
            </div>

            <button className="ui primary small submit button">Apply filters</button>

            <div className="ui error message">
              <div className="header">Could not filter transactions</div>
              <p>{this.state.error}</p>
            </div>
          </form>
          <hr/>
        </div>*/}

        <div className="ui grid">
          <div className="ten wide column" style={{paddingBottom: 0}}>
            Transactions: {this.state.data ? this.state.data.total_count : '-'}
          </div>
          <div className="six wide column right aligned" style={{paddingBottom: 0}}>
            <a className={"ui tiny compact primary button" + (this.state.exporting ? " loading" : "")}
                style={{margin: 0}}
                onClick={this.onExport.bind(this)}>Export to CSV</a>
            <a ref="csvexport" style={{display: "none"}} target="_blank"></a>
          </div>

          <div className="sixteen wide column">
            <table className='ui small compact table'>
              <thead>
                <tr>
                  <th className="two wide">Date</th>
                  <th className="five wide">Tx ID</th>
                  <th className="four wide">Address</th>
                  <th className="two wide">{currency} Amount</th>
                  <th className="two wide">Note</th>
                  <th className="one wide">Confirmations</th>
                </tr>
              </thead>
              <tbody>{txlist}</tbody>
            </table>
          </div>

          <div className="sixteen wide column">
            {pagination}
          </div>

          <div className="sixteen wide column">
            <a className="ui button" onClick={browserHistory.goBack}>Go back</a>
          </div>
        </div>

      </div>
    );
  }
}

WalletHistory.contextTypes = {
  router: routerShape.isRequired
};


export default WalletHistory;
