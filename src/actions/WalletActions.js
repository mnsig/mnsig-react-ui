import AppDispatcher from '../dispatcher';
import WalletConstants from '../constants/WalletConstants';


export default {

  /**
   * @param {object} user
   * @param {string} wallet
   */
  getKeys: function(user, wallet) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: WalletConstants.GET_KEYS,
      instance: user.instance,
      token: user.token,
      data: {wallet: wallet}
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   */
  getTransactions: function(user, wallet, params) {
    if (!user.token) {
      return;
    }
    var data = params || {};
    data['wallet'] = wallet;
    AppDispatcher.dispatch({
      actionType: WalletConstants.GET_TRANSACTIONS,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   */
  exportTransactions: function(user, wallet) {
    if (!user.token) {
      return;
    }
    var data = {wallet: wallet, export: true};
    AppDispatcher.dispatch({
      actionType: WalletConstants.EXPORT_TRANSACTIONS,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

};
