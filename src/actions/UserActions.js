import crypto from 'crypto';
import AppDispatcher from '../dispatcher';
import UserConstants from '../constants/UserConstants';


export default {

  /**
   * @param {object} self
   * @param {string} url
   * @param {object} data
   * @param {string} pwd
   */
  login: function(instance, path, data, pwd) {
    data.hashed_pwd = crypto.createHash('sha256').update(pwd).digest('hex');

    AppDispatcher.dispatch({
      actionType: (path === 'login') ? UserConstants.DO_LOGIN : UserConstants.DO_SIGNUP,
      instance: instance,
      data: data
    });
  },

  /**
   * @param {object} user
   */
  logout: function(user) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: UserConstants.DO_LOGOUT,
      instance: user.instance,
      token: user.token
    });
  },

  /**
   * @param {object} user
   */
  getUserData: function(user) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: UserConstants.GET_USERDATA,
      instance: user.instance,
      token: user.token
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  createWallet: function(user, data) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: UserConstants.CREATE_WALLET,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  addKey: function(user, data) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: UserConstants.ADD_KEY,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   */
  createAddress: function(user, wallet) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: UserConstants.DERIVE_ADDRESS,
      instance: user.instance,
      token: user.token,
      data: {wallet: wallet}
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   * @param {object} data
   */
  addAPIToken: function(user, wallet, data) {
    if (!user.token) {
      return;
    }
    data.wallet = wallet;
    AppDispatcher.dispatch({
      actionType: UserConstants.CREATE_API_TOKEN,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   * @param {object} data
   */
  deleteAPIToken: function(user, wallet, data) {
    if (!user.token) {
      return;
    }
    data.wallet = wallet;
    AppDispatcher.dispatch({
      actionType: UserConstants.DELETE_API_TOKEN,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {string} wallet
   * @param {object} data
   */
  addUserWallet: function(user, wallet, data) {
    if (!user.token) {
      return;
    }
    data.wallet = wallet;
    AppDispatcher.dispatch({
      actionType: UserConstants.ADD_WALLET_USER,
      instance: user.instance,
      token: user.token,
      data: data
    });
  }

};
