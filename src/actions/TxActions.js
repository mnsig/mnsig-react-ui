import AppDispatcher from '../dispatcher';
import TxConstants from '../constants/TxConstants';


export default {

  /**
   * @param {object} user
   * @param {object} data
   */
  startProposal: function(user, data) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.START_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  prepareProposal: function(user, proposal) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.PREPARE_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: proposal
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  cancelProposal: function(user, data) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.CANCEL_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  getProposal: function(user, data) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.GET_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: data
    });
  },

  /**
   * @param {object} user
   * @param {object} data
   */
  sendProposalSig: function(user, proposal, xpriv) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.SIGN_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: proposal,
      xpriv: xpriv
    });
  },


  /**
   * @param {object} user
   * @param {object} proposal
   */
  broadcastProposal: function(user, proposal) {
    if (!user.token) {
      return;
    }
    AppDispatcher.dispatch({
      actionType: TxConstants.BROADCAST_PROPOSAL,
      instance: user.instance,
      token: user.token,
      data: proposal
    });
  },

};
