'use strict';

import React from 'react';
import Router from 'react-router/lib/Router';
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';
import { useRouterHistory } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import useScroll from 'scroll-behavior/lib/useStandardScroll';

import Index from './pages/index';
import Dashboard from './pages/dashboard';
import Login from './pages/login';
import Logout from './pages/logout';
import Wallet from './pages/views/wallet';
import WalletCreate from './pages/wallet_create';
import WalletAddUser from './pages/wallet_adduser';
import WalletKeyAdd from './pages/wallet_addkey';
import WalletAPITokenAdd from './pages/wallet_addtoken';
import StartWithdraw from './pages/start_withdraw';
import ViewProposal from './pages/view_proposal';
import History from './pages/history';
import RawPassword from './pages/misc/raw_password';
import { requireAuth } from './utils/auth';

const appHistory = useScroll(useRouterHistory(createBrowserHistory))();

const routes = (
  <Router history={appHistory}>
    <Route path="/" component={Index}>
      <IndexRoute component={Dashboard}/>
      <Route path="login" component={Login}/>
      <Route path="logout" component={Logout}/>
      <Route path="create" component={WalletCreate} onEnter={requireAuth}/>
      <Route path="wallet/:wallet" component={Wallet} onEnter={requireAuth}/>
      <Route path="key/:wallet/:derive/:network" component={WalletKeyAdd} onEnter={requireAuth}/>
      <Route path="apitoken/:wallet/:currency" component={WalletAPITokenAdd} onEnter={requireAuth}/>
      <Route path="user/:wallet" component={WalletAddUser} onEnter={requireAuth}/>
      <Route path="proposal/:wallet" component={StartWithdraw} onEnter={requireAuth}/>
      <Route path="proposal/:wallet/:derive/:id" component={ViewProposal} onEnter={requireAuth}/>
      <Route path='history/:wallet' component={History} onEnter={requireAuth}/>
      <Route path='history/:wallet/:page' component={History} onEnter={requireAuth}/>

      <Route path="misc/rawpw" component={RawPassword}/>
    </Route>
  </Router>
);

export default routes;
