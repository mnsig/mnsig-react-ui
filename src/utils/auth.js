import UserStore from '../stores/UserStore';

export function requireAuth(nextState, replace) {
  if (!UserStore.getUser().token) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}
